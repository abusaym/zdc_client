import os
import sys
import requests

SERVER_URL = os.environ.get('SERVER_URL', 'http://172.17.0.2:8000/recieve/')

def send_message():
    try:
        print('Enter message to send to server: ', end='', flush=True)
        data = sys.stdin.readline().strip()
    except KeyboardInterrupt:
        return False
    if not data:
        return True
    if data == 'exit':
        return False
    payload = {'message': data}
    try:
        response = requests.post(SERVER_URL, data=payload)
    except requests.exceptions.ConnectionError:
        print('Error connecting to server at', SERVER_URL)
        return False
    if response.status_code == 200:
        print('Message sent successfully.')
    else:
        print('Error sending message. Status code:', response.status_code)
    return True

if __name__ == '__main__':
    while send_message():
        pass
