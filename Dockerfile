# Use Python 3.9 as the base image
FROM python:3.8

# Set the working directory to /app
WORKDIR /app

# Copy the client script and requirements file to the container
COPY client.py .
COPY requirements.txt .

# Install required dependencies
RUN pip install -r requirements.txt

# Start the client script when the container starts
CMD ["python", "client.py"]
